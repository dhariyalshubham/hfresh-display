export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    
    {
      name: 'Stores',
      url: '/stores',
      icon: 'icon-home',
      children: [
        
      ],
    },     
    {
      title: true,
      name: 'Extras Links',
    },
    {
      name: 'Pages',
      url: '/pages',
      icon: 'icon-star',
      children: [
        {
          name: 'Login',
          url: '/login',
          icon: 'icon-star',
        },
        {
          name: 'Register',
          url: '/register',
          icon: 'icon-star',
        },
        {
          name: 'Error 404',
          url: '/404',
          icon: 'icon-star',
        },
        {
          name: 'Error 500',
          url: '/500',
          icon: 'icon-star',
        },
      ],
    },
    {
      name: 'Visit Us',
      url: 'http://hfresh.com.au',
      icon: 'icon-globe',
      class: 'mt-auto',
      variant: 'success',
    },
  ],
};
