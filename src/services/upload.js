import axios from "axios";
const URL = "http://localhost:8080/"||"https://4wj88258q9.sse.codesandbox.io/";


//always send the userToken in header in every request. Required for authentication
const imageUploadServer = async (images)=>{
    console.log("images check");
    console.log(images);
    const response = await axios.post(URL+"upload/imageUpload",images,{
        headers: { 
            Authorization: "Bearer " + window.localStorage.getItem("userToken") }
    });
    console.log(response.data);
    return response.data;
    
};



export {imageUploadServer};