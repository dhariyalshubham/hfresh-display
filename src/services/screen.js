import axios from "axios";
const URL = "http://localhost:8080/"||"https://4wj88258q9.sse.codesandbox.io/";


//always send the userToken in header in every request. Required for authentication
const createScreen = async (screen)=>{
    const response = await axios.post(URL+"screen/create",{
        screen
    },{
        headers: { 
            Authorization: "Bearer " + window.localStorage.getItem("userToken") }
    });
    
    return response.data;
    
};

const getScreens = async(storeId)=>{
    const response = await axios.get(URL+"screen/getScreens/"+storeId,{
        headers: { 
            Authorization: "Bearer " + window.localStorage.getItem("userToken") }
    });
    return response.data;
}


export {createScreen,getScreens};