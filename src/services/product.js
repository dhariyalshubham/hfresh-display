const axios = require("axios");

const getProducts = async (productString)=>{
    if(productString.length>1){
        const params = {
            productString
        };
        
        const response = await axios.get('http://localhost:8080/products', { params });
        
        return response.data;
    }
    

}

export {getProducts};
