import axios from "axios";
const URL = "http://localhost:8080/"||"https://4wj88258q9.sse.codesandbox.io/";


//always send the userToken in header in every request. Required for authentication
const createDisplay = async (display)=>{
    const response = await axios.post(URL+"display/create",{
        display
    },{
        headers: { 
            Authorization: "Bearer " + window.localStorage.getItem("userToken") }
    });
    
    return response.data;
    
};

const getDisplays = async(screenId)=>{
    const response = await axios.get(URL+"display/getDisplays/"+screenId,{
        headers: { 
            Authorization: "Bearer " + window.localStorage.getItem("userToken") }
    });
    return response.data;
}


export {createDisplay,getDisplays};