import axios from "axios";
const URL = "http://localhost:8080/"||"https://4wj88258q9.sse.codesandbox.io/";


//always send the userToken in header in every request. Required for authentication
const createStore = async (store)=>{
    console.log("create store");
    console.log(store);
    const response = await axios.post(URL+"store/create",{
        store
    },{
        headers: { Authorization: "Bearer " + window.localStorage.getItem("userToken") }
    });
    return response.data;
    console.log(response);
    
};

const getStores = async()=>{
    const response = await axios.get(URL+"store/getStores/"+window.localStorage.getItem("userId"),{
        headers: { Authorization: "Bearer " + window.localStorage.getItem("userToken") }  
    },);
    console.log("got stores");
    return response.data;
    
}


export {createStore,getStores};