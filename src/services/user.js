import axios from "axios";
const URL = "http://localhost:8080/"||"https://4wj88258q9.sse.codesandbox.io/";

window.localStorage.setItem("lastname", "Smith");
//always send the userToken in header in every request. Required for authentication
const login = async (userData)=>{
    const response = await axios.post(URL+"user/login",{
        userData
    });
    
    console.log(response);
    window.localStorage.setItem("userId", response.data.user.userId);
    window.localStorage.setItem("userToken", response.data.user.userToken);
    return response.status==200;
    
};
const signup = async (userData)=>{
    
    const response = await axios.post(URL+"user/signup",{
        userData
    });
    
    console.log(response.data.user.userId);
    console.log(response.data.user.userToken);
    window.localStorage.setItem("userId", response.data.user.userId);
    window.localStorage.setItem("userToken", response.data.user.userToken);
};



export {login,signup};