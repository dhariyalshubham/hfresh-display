import React, { Component } from 'react';
import { Container, Row, Col, InputGroupAddon, InputGroup, Form, FormGroup, Label, Input, CustomInput, Button } from 'reactstrap'; 
import { TimePicker, TreeSelect,Select,Spin } from 'antd';
import {Form as AntdForm} from 'antd';
import {Button as AntdButton } from 'antd';
import moment from 'moment';
import {getProducts} from "../../services/product";
import {createDisplay} from "../../services/display";
import {imageUploadServer} from "../../services/upload";
import 'antd/dist/antd.css'; 

const Option = Select.Option;

const FormItem = AntdForm.Item;

//TreeSelect data
const SHOW_PARENT = TreeSelect.SHOW_PARENT;

class Screen1 extends Component { 
    constructor(props){
        super(props);
        this.state = {
            productListData: [],
            productList: [],
            screen: this.props.match.params.screenId,
            store: this.props.match.params.storeId,
            delay: 0,
            productName :"",
            productPrice  :"",
            productCategory:"",
            productImages :[],
            imageUploading: false,
            uploadedImages: []
        }
    }
    fetchProducts = async(x)=>{
        if(x.length>1){
            this.setState({  fetching: true });
            const productListData = await getProducts(x);
            console.log("gots products");
            console.log(productListData);
            this.setState({
                productListData
            })
        }
        
        
    }
    
    handleChange = (productList) => {
        productList = productList.map((product)=>{
            return{
                id: product.split("&#&")[0],
                name: product.split("&#&")[1]
            }
        });
        this.setState({
          productList,
          productListData: [],
          fetching: false,
        });
        console.log(productList);
      }
   
    
      onChange = (event)=> {
        
        this.setState(
            {        
                [event.target.name]: event.target.value
            }
            
        );
      }
      imageUpload = async (e)=>{
        const files = Array.from(e.target.files)
        console.log(files);
        this.setState({ imageUploading: true })
    
        const formData = new FormData()
    
        files.forEach((file, i) => {
          formData.append(i, file)
        })
        
        const images = await imageUploadServer(formData);
        console.log("uploaded images");
        this.setState({
            imageUploading: false,
            productImages: [...this.state.productImages,...images]
        })
        
      }
      submitDynamic = async ()=>{
          await createDisplay({
              displayType: "dynamic",
              products: this.state.productList,
              screen: this.state.screen,
              delay: this.state.delay,
              store: this.state.store
          });
          alert("dynamic display submiited succesfully");
          this.setState({
            productListData: [],
            productList: [],
            screen: this.props.match.params.screenId,
            store: this.props.match.params.storeId,
            delay: 0,
            productName :"",
            productPrice  :"",
            productCategory:"",
            productImages :[],
            imageUploading: false,
            uploadedImages: []
          })
      }
      submitStatic = async ()=>{
        console.log("static form")
        
        await createDisplay({
            displayType: "static",
            screen: this.state.screen,
            productName     :this.state.productName,
            productPrice    :this.state.productPrice,
            productCategory : this.state.productCategory,
            productImages    :this.state.productImages    ,
            store: this.state.store
        });
        alert("static display submiited succesfully");
          this.setState({
            productListData: [],
            productList: [],
            screen: this.props.match.params.screenId,
            store: this.props.match.params.storeId,
            delay: 0,
            productName :"",
            productPrice  :"",
            productCategory:"",
            productImages :[],
            imageUploading: false,
            uploadedImages: []
          })
    }

    render() {
       
        return (
            <div className="Displayname">
                <Container>
                    <Row>
                        <Col xs="6">
                            <h3>Static Image</h3>
                            <Form onSubmit={this.submitSingleScreen}>
                                <FormGroup>
                                    <Label for="productName">Product Name</Label>
                                    <Input valid type="text" name="text" name="productName" placeholder="Name of Product" 
                                        value={this.state.productName}
                                        onChange = {this.onChange}
                                        />            
                                </FormGroup>  
                                <FormGroup>
                                    <Label for="productName">Product Price ($)</Label>
                                    <InputGroup>
                                        <InputGroupAddon addonType="prepend">$</InputGroupAddon>
                                        <Input valid placeholder="Amount" name="productPrice" type="number" value={this.state.productPrice} step="1" 
                                            onChange = {this.onChange}
                                        />
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="productCategory">Product Category</Label>
                                    <Input valid type="select" name="productCategory" onChange={this.onChange} value={this.state.productCategory}>
                                        <option value={"fruits"}>Fruits</option>
                                        <option value={"vegetables"}>Vegetables</option>
                                        <option value={"others"}>Others</option>
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="selectImage">Select Image</Label>
                                    
                                    <CustomInput onChange={this.imageUpload} valid type="file" id="selectImage" name="customFile" multiple/>
                                </FormGroup>
                                <FormGroup>
                                    {this.state.imageUploading?<Spin />:<Row gutter={8}>{this.state.productImages.map((productImage)=>{
                                        return (
                                            
                                                <Col span={4} key={productImage}>
                                                    <img src={productImage} width="60" height="60"/>
                                                </Col>
                                                
                                                
                                        )
                                    })}</Row>}
                                </FormGroup>
                                <FormGroup>
                                    <Button onClick={this.submitStatic}>Submit</Button>
                                </FormGroup>
                            </Form>
                        </Col>

                        <Col xs="6">
                            <h3>Dynamic Slider</h3>
                            <AntdForm onSubmit={this.submitMultiScreen}>
                            <FormGroup>
                                    <Label >Search Products</Label>
                                
                                <Select
                                    mode="multiple"
                                    style={{ width: '100%' }}
                                    placeholder="Please select"
                                    filterOption={false}
                                    onChange={this.handleChange}
                                    notFoundContent={this.state.fetching ? <Spin size="small" /> : null}
                                    onSearch={this.fetchProducts}
                                >
                                     {this.state.productListData.map(d => <Option key={d.id+"&#&"+d.post_content}>{d.post_content}</Option>)}
                                </Select>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="delayTime">Delay Time(seconds)</Label>
                                    <InputGroup>
                                        <InputGroupAddon addonType="prepend">seconds</InputGroupAddon>
                                        <Input valid placeholder="DelayTime" type="number" step="1" name="delay" value={this.state.delay} onChange={this.onChange}/>
                                    </InputGroup>
                                </FormGroup>
                                
                                <FormItem>
                                    <AntdButton type="primary" htmlType="submit" onClick={this.submitDynamic}>Submit</AntdButton>
                                </FormItem>
                            </AntdForm>
                        </Col>
                    </Row>                    
                </Container>                           
            </div>
        );
    }
}

export default Screen1;