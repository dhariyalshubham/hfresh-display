import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import {login} from "../../../services/user";
import { Redirect } from 'react-router-dom';
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {userName: "",password:"",loginStatus:false};
  }
  handleChange=(event)=> {
    this.setState({[event.target.name]: event.target.value});
  }
  loginHandler=async  ()=>{
    
    let loginStatus = await login({
      userName: this.state.userName, "password": this.state.password
    });
    
    //login call karo return value ko store karo
    //success= login call
    
    this.setState({  loginStatus });
    
    
  }
  registerClick=()=>{
    this.setState({ is_Register_Click:true });
  }
  render() {
    if(window.localStorage.getItem("userToken")){
      return (<Redirect to="/dashboard" />); 
    }
    if(this.state.is_Register_Click){
      return (<Redirect to="/register" />);
    }
    if (this.state.loginStatus){
      return(<Redirect to="/dashboard" />);
    }else{
      return (
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="8">
                <CardGroup>
                  <Card className="p-4">
                    <CardBody>
                      <Form>
                        <h1>Login</h1>
                        <p className="text-muted">Sign In to your account</p>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input type="text" placeholder="Username" autoComplete="username" name="userName"
                            value={this.state.userName} onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-4">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input type="password" placeholder="Password" autoComplete="current-password"
                            name="password"
                            value={this.state.password} onChange={this.handleChange}
                          />
                        </InputGroup>
                        <Row>
                          <Col xs="6">
                            <Button color="primary" className="px-4"
                              onClick={this.loginHandler}>
                              Login
                            </Button>
                          </Col>
                          <Col xs="6" className="text-right">
                            <Button color="link" className="px-0">Forgot password?</Button>
                          </Col>
                        </Row>
                      </Form>
                    </CardBody>
                  </Card>
                  <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
                    <CardBody className="text-center">
                      <div>
                        <h2>Sign up</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                        <Button color="primary" className="mt-3" onClick={this.registerClick} active>Register Now!</Button>
                      </div>
                    </CardBody>
                  </Card>
                </CardGroup>
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
  }
}

export default Login;