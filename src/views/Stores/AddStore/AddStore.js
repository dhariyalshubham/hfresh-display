import React, { PureComponent } from 'react';

import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import {createStore} from "../../../services/store";
import { Store } from 'mqtt';
class AddStore extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {name: "",state:"",country:"",suburb:"",street:"",address:"",
    pincode:"",
    user:""
    };
  }
  registerStore = async ()=>{
    let userId = window.localStorage.getItem("userId");
    if(!userId){
        alert("PLease login to create a store")
    }
    let createdStore = await createStore({
        name: this.state.name,
        state: this.state.state,
        country: this.state.country,
        suburb: this.state.suburb,
        street: this.state.street,
        address: this.state.address,
        pincode: this.state.pincode,
        user: userId
    });
    this.props.history.push('/singleStore/'+createdStore._id)
  }
  handleChange=(event)=> {
    console.log(event.target.name+" "+ event.target.value);
    this.setState({[event.target.name]: event.target.value});
  }
  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Create Store</h1>
                      <p className="text-muted">Create Store</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Name" autoComplete="Name" name="name"
                          value={this.state.name} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="state" autoComplete="state" 
                          name="state"
                          value={this.state.state} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="country" autoComplete="country" 
                          name="country"
                          value={this.state.country} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="suburb" autoComplete="suburb" 
                          name="suburb"
                          value={this.state.suburb} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="pincode" autoComplete="pincode" 
                          name="pincode"
                          value={this.state.pincode} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" 
                          onClick={this.registerStore}>
                            Create Store
                            </Button>
                        </Col>
                        
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default AddStore;
