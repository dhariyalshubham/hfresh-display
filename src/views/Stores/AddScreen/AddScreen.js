import React, { PureComponent } from 'react';
import {login} from "../../../services/user";
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import {createScreen} from "../../../services/screen";
class AddScreen extends PureComponent {
  constructor(props) {
    super(props);  
    this.state = {
        rid: "",
        rurl:"",
        screenName:"",
        store:""
    };
  }
  registerScreen = ()=>{
    let userId = window.localStorage.getItem("userId");
    if(!userId){
        alert("PLease login to create a store")
    }
    createScreen({
        rid: this.state.rid,
        rurl:this.state.rurl,
        screenName:this.state.screenName,
        store:this.state.store
    });
  }
  handleChange=(event)=> {
    console.log(event.target.name+" "+ event.target.value);
    this.setState({[event.target.name]: event.target.value});
  }
  
  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Login</h1>
                      <p className="text-muted">Create Screen</p>
                      
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="screenName" autoComplete="screenName" 
                          name="screenName"
                          value={this.state.screenName} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="rid" autoComplete="rid" 
                          name="rid"
                          value={this.state.rid} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="rurl" autoComplete="rurl" 
                          name="rurl"
                          value={this.state.rurl} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" 
                          onClick={()=>login({
                            userName:this.state.userName,"password":this.state.password
                            })}>
                            Login
                            </Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.</p>
                      <Button color="primary" className="mt-3" active>Register Now!</Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default AddScreen;
