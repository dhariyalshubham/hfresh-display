import React, { PureComponent } from 'react';
import { Modal, Row, Button,ModalBody,ModalFooter,ModalHeader,Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import {} from "antd";
import { Link } from 'react-router-dom'
import axios from 'axios';
import {getScreens,createScreen} from "../../../services/screen";
class SingleStore extends PureComponent{

    //state={screens:[]}
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false,
            confirmLoading: false,
            screens: [],
            
                screenName: "",
                rid: "",
                rurl: "",
                store: this.props.match.params.storeId
            
        }
    }
    
    componentWillMount = async ()=>{
        
        const screens = await getScreens(this.props.match.params.storeId);
        
        if(screens.length && screens.map){
            this.setState({
                screens: screens.map((screen)=>{
                    return {
                        ...screen
                    }
                    
                })
            })
        }
        
    }
    
    componentDidUpdate= async (prevProps)=>{
      
      if(this.props.match.params.storeId!=prevProps.match.params.storeId){
        
        const screens = await getScreens(this.props.match.params.storeId);
        
        if(screens.length && screens.map){
            this.setState({
                screens: screens.map((screen)=>{
                    return {
                        ...screen
                    }
                    
                })
            })
        }else{
          this.setState({
            screens: []
          })
        }
      }
      
    }
    showModal = () => {
        this.setState({
          modalVisible: true,
        });
      }
    handleOk = () => {
        
        this.setState({
            modalVisible: false,
            confirmLoading: false,
          });
      }
    createScreen = async ()=>{
        let screen = await createScreen({
            screenName: this.state.screenName,
            rid: this.state.rid,
            rurl: this.state.rurl,
            store: this.state.store
        });
        this.setState((prevState)=>{
            return {
                screens: [...prevState.screens,{...screen}],
                modalVisible: false
            }
        })
    }
      handleCancel = () => {
        console.log('Clicked cancel button');
        this.setState({
          modalVisible: false,
        });
      }

      handleChange=(event)=> {
        
        this.setState(
             {        
                [event.target.name]: event.target.value
                }
            
        );
      }
      
    render =()=>{
        return(
            <div>
                <Container>
                    <Row>
                        {this.state.screens.map(({_id,screenName})=>{
                            console.log(_id);
                            let url=`/singleScreen/${this.state.store}/${_id}`;
                            console.log(url);
                            return(
                                <Col md="2" key={_id}>
                                    <Link to={url}>
                                        <Button><i className="fa fa-desktop fa-5x"></i></Button>
                                        <h3>{screenName}</h3>
                                    </Link>
                                </Col>
                            );
                        })}
                        <Col md="2">
                                    
                                        <Button onClick={this.showModal}><i className="fa fa-desktop fa-5x"></i></Button>
                                        <h3>{"Add Screen"}</h3>
                                       
                                </Col>
                    </Row>
                    <Modal title="Title"
                            isOpen={this.state.modalVisible}
                            
                    >
                    <ModalHeader>{"header"}</ModalHeader>
                        <ModalBody>
                        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Create Screen</h1>
                      
                      
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="screenName" autoComplete="screenName" 
                          name="screenName"
                          value={this.state.screenName} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="rid" autoComplete="rid" 
                          name="rid"
                          value={this.state.rid} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="rurl" autoComplete="rurl" 
                          name="rurl"
                          value={this.state.rurl} onChange={this.handleChange} 
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4" 
                          onClick={this.createScreen}>
                            Create Screen
                            </Button>
                        </Col>
                        
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                
            </Col>
          </Row>
        </Container>
      
                        </ModalBody>
                        <ModalFooter>
                        <Button color="primary" className="px-4" 
                          onClick={this.handleCancel}>
                            Close
                            </Button>
                        </ModalFooter>
                    </Modal>
                </Container>
                
            </div>
        );
    }
    // }
}

export default SingleStore;