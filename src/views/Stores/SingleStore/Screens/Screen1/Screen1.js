import React, { Component } from 'react';
import { Container, Row, Col, InputGroupAddon, InputGroup, Form, FormGroup, Label, Input, CustomInput, Button } from 'reactstrap'; 
import { TimePicker, TreeSelect,Select,Spin } from 'antd';
import {Form as AntdForm} from 'antd';
import {Button as AntdButton } from 'antd';
import moment from 'moment';
import {getProducts} from "../../../../../services/product";
import 'antd/dist/antd.css'; 

const Option = Select.Option;

const FormItem = AntdForm.Item;

//TreeSelect data
const SHOW_PARENT = TreeSelect.SHOW_PARENT;

const treeData = [{
  title: 'Fruits',
  value: '0-0',
  key: '0-0',
  children: [{
    title: 'Child Node1',
    value: '0-0-0',
    key: '0-0-0',
  }],
}, {
  title: 'Vegetables',
  value: '0-1',
  key: '0-1',
  children: [{
    title: 'Cucumber',
    value: '0-1-0',
    key: '0-1-0',
  }, {
    title: 'Potato',
    value: '0-1-1',
    key: '0-1-1',
  }, {
    title: 'Tomato',
    value: '0-1-2',
    key: '0-1-2',
  }],
}];

    //Time Picker function
    function onChange(time, timeString) {
        console.log(time, timeString);
      }   


const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}
class Screen1 extends Component { 
    constructor(props){
        super(props);
        this.state = {
            productListData: [],
            productList: []
        }
    }
    fetchProducts = async(x)=>{
        if(x.length>1){
            this.setState({  fetching: true });
            const productListData = await getProducts(x);
            console.log("gots products");
            console.log(productListData);
            this.setState({
                productListData
            })
        }
        
        
    }
    componentDidMount(){
        let id = this.props.location.search.split('=')[1];
        console.log(parseInt(id));
        this.setState({
            id
        })
    }
    handleChange = (productList) => {
        this.setState({
          productList,
          productListData: [],
          fetching: false,
        });
        console.log(productList);
      }
   
    
      onChange = (value) => {
        console.log('onChange ', value);
        this.setState({ value });
      }

      submitSingleScreen=()=>{
          alert("data single screen= "+this.state.id );
          // axios.post('/quotations/xhr', {
        //     params: {
        //         d: data.d,
        //         qid: data.qid,
        //     }
        // })
        //     .then((res) => {
        //        //redirect multiple screen page jaha sari screen dikh rahi h
        //     }).catch((err) => {
        //         return err
        //     }) 
      }

    submitMultiScreen = () => {
        alert("data multi screen= " + this.state.id);
        // axios.post('/quotations/xhr', {
        //     params: {
        //         d: data.d,
        //         qid: data.qid,
        //     }
        // })
        //     .then((res) => {
        //        //redirect multiple screen page jaha sari screen dikh rahi h
        //     }).catch((err) => {
        //         return err
        //     }) 
    }

    render() {
        const tProps = {
            treeData,
            value: this.state.value,
            onChange: this.onChange,
            treeCheckable: true,
            showCheckedStrategy: SHOW_PARENT,
            searchPlaceholder: 'Please select',
            style: {
              width: 300,
            },
          };

        return (
            <div className="Displayname">
                <Container>
                    <Row>
                        <Col xs="6">
                            <h3>Static Image</h3>
                            <Form onSubmit={this.submitSingleScreen}>
                                <FormGroup>
                                    <Label for="productName">Product Name</Label>
                                    <Input valid type="text" name="text" id="productName" placeholder="Name of Product" />                                      
                                </FormGroup>  
                                <FormGroup>
                                    <Label for="productName">Product Price ($)</Label>
                                    <InputGroup>
                                        <InputGroupAddon addonType="prepend">$</InputGroupAddon>
                                        <Input valid placeholder="Amount" type="number" step="1" />
                                    </InputGroup>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="productCategory">Product Category</Label>
                                    <Input valid type="select" name="select" id="productCategory">
                                        <option>Fruits</option>
                                        <option>Vegetables</option>
                                        <option>Others</option>
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="selectImage">Select Image</Label>
                                    <CustomInput valid type="file" id="selectImage" name="customFile" />
                                </FormGroup>
                                <FormGroup>
                                    <Button>Submit</Button>
                                </FormGroup>
                            </Form>
                        </Col>

                        <Col xs="6">
                            <h3>Dynamic Slider</h3>
                            <AntdForm onSubmit={this.submitMultiScreen}>
                                <FormItem label="Multiple Selection">
                                    <TreeSelect {...tProps} />                                </FormItem>
                                <FormItem>
                                <Select
                                    mode="multiple"
                                    style={{ width: '100%' }}
                                    placeholder="Please select"
                                    filterOption={false}
                                    onChange={this.handleChange}
                                    notFoundContent={this.state.fetching ? <Spin size="small" /> : null}
                                    onSearch={this.fetchProducts}
                                >
                                     {this.state.productListData.map(d => <Option key={d.id}>{d.post_content}</Option>)}
                                </Select>
                                </FormItem>
                                <FormItem label="Delay Time">
                                    <TimePicker onChange={onChange} defaultOpenValue={moment('00:00:00', 'HH:mm:ss')} />
                                </FormItem>                                    
                                <FormItem>
                                    <AntdButton type="primary" htmlType="submit">Submit</AntdButton>
                                </FormItem>
                            </AntdForm>
                        </Col>
                    </Row>                    
                </Container>                           
            </div>
        );
    }
}

export default Screen1;