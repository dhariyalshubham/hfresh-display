import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
// routes config
import routes from '../../routes';
import DefaultAside from './DefaultAside';
import DefaultFooter from './DefaultFooter';
import DefaultHeader from './DefaultHeader';
import {getStores} from "../../services/store";




class DefaultLayout extends Component {

  constructor(props){
    super(props);
    this.state = {
      navigation
    };
  }
  componentWillMount = async()=>{
    const stores = await getStores();
    let storesNav = navigation.items[1];
    
    
    storesNav.children = storesNav.children.concat(stores.map((store)=>{
      return {
        name:store.name,
        url:"/singleStore/"+store._id,
        icon: "icon-puzzle"
      }
      
    }))
    storesNav.children.push(
    {
      name: 'Add Store',
      url: '/stores/add_store',
      icon: 'icon-puzzle',
    });
    
    navigation.items[1] = storesNav;
    this.setState({
      navigation
    })
  }
  render() {
    if(!window.localStorage.getItem("userToken")){
      return <Redirect to="/login"></Redirect>
    }
    return (
      <div className="app">
        <AppHeader fixed>
          <DefaultHeader />
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <AppSidebarNav navConfig={this.state.navigation} {...this.props} />
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes}/>
            <Container fluid>
              <Switch>
                {routes.map((route, idx) => {
                    return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                        <route.component {...props} />
                      )} />)
                      : (null);
                  },
                )}
                <Redirect from="/" to="/dashboard" />
              </Switch>
            </Container>
          </main>
          <AppAside fixed>
            <DefaultAside />
          </AppAside>
        </div>
        <AppFooter>
          <DefaultFooter />
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
